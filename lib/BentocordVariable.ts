export enum BentocordVariable {
	BENTOCORD_TOKEN = 'BENTOCORD_TOKEN',
	BENTOCORD_COMMAND_PREFIX = 'BENTOCORD_COMMAND_PREFIX',
	BENTOCORD_BUILTIN_COMMANDS = 'BENTOCORD_BUILTIN_COMMANDS',
	BENTOCORD_STORAGE_ENTITY = 'BENTOCORD_STORAGE_ENTITY',
	BENTOCORD_PERMISSIONS_ENTITY = 'BENTOCORD_PERMISSIONS_ENTITY',
	BENTOCORD_BOT_OWNERS = 'BENTOCORD_BOT_OWNERS',
}
