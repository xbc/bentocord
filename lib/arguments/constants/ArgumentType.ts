export enum ArgumentType {
	// Primitives
	STRING, STRINGS,
	NUMBER, NUMBERS,
	BOOLEAN, BOOLEANS,

	// Discord User
	USER, USERS,

	// Discord Member
	MEMBER, MEMBERS,
	RELEVANT, RELEVANTS,

	// Discord Channel
	CHANNEL, CHANNELS,

	// Discord Role
	ROLE, ROLES,

	// Discord Guild
	GUILD, GUILDS,

	// Discord Emoji
	EMOJI, EMOJIS,
}
