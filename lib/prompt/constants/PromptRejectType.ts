export enum PromptRejectType {
	CANCEL = 'CANCEL',
	TIMEOUT = 'TIMEOUT',
	RETRY_LIMIT = 'RETRY_LIMIT',
}
